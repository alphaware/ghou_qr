
/*---------------ACTIVAR DEBUG_ACTIVE ACTIVA MONITORIZACION DE LOS STATS DE TAREAS--------------*/
#define OS_THREAD_LIBSPACE_NUM 14 
#include "stdlib.h"
#include "string.h"
#include "mbed.h"
#include "rtos.h"
#include "easy-connect.h"
#include "ghou_setup.h"
#include "mbed_trace.h"
#include "nsapi_types.h"
#include "SDBlockDevice.h"
#include "http_response_builder.h"
#include "https_request.h"
#include "TCPServer.h"
#include "TCPSocket.h"

/*-----------------ESTADISTICAS------------*/

#define MAX_THREAD_INFO 13
/*#define QR_MASTER   "xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx" */


typedef HttpResponse ParsedHttpRequest;
typedef struct {
    int     direction;   /* direccion */ 
    char    token[TOKEN_LENGTH]="";   /* uuid  */
} mail_t;


Serial pc(USBTX, USBRX);
DigitalOut led(LED1);
DigitalOut led_thread(LED2);
DigitalOut led_server(LED3);


DigitalOut RELE1(RELE_1);
DigitalOut RELE2(RELE_2);
#ifdef CONCENTRADOR_MULTIPLE
DigitalOut RELE3(RELE_3);
DigitalOut RELE4(RELE_4);
#endif



Mail<mail_t, TOKEN_MAIL_MAX> mail_box;
EventFlags isConnected;
//----------------------------TAREAS-----------------------------------------
Thread thread_led;
Thread thread_httpsGet;
Thread thread_server;
Thread thread_deleteTokens;
//-----------------------------MUTEX-----------------------------------------
Mutex delete_token;
//---------------------------VARIABLES---------------------------------------
int con_status=0;               //flag para saber si hay modo online o no
int t_https=TIME_REQUEST;       //tiempo en segundos del https_task
string *json_base;              //base de datos actual de tokens
const char* j;                  //tokens actual entrada
int* kpt;
char delete_uuid[TOKEN_LENGTH]="";

//comentario revisar status antes de actualizar 
void dump_response(HttpResponse* res) {
     printf("Status: %d - %s\n\r", res->get_status_code(), res->get_status_message().c_str());
    /*
    printf("Headers:\n\r");
    for (size_t ix = 0; ix < res->get_headers_length(); ix++) {
        printf("\t%s: %s\n\r", res->get_headers_fields()[ix]->c_str(), res->get_headers_values()[ix]->c_str());
    }
    */
    //imprime el body
    //printf("\nBody (%lu bytes):\n\n%s\n\r", res->get_body_length(), res->get_body_as_string().c_str());
    printf ("\nBody (%lu bytes)\n\r",(unsigned long) res->get_body_length());
}

void dump_response_db(HttpResponse* res) {
    printf("Status: %d - %s\n\r", res->get_status_code(), res->get_status_message().c_str());
    wait(1);
    if(!res || res->get_status_code()!=RESPONSE_OK){
            printf("Base de datos no actualizada\n\r");
        }
        else{
            /*
            json_base =res->get_body_as_string().c_str();
            j = json_base.c_str();
            printf("Base de datos actualizada\n\r");
            */
            j=res->get_body_as_string().c_str();
            // Read the data block from the device
                printf("Base de datos actualizada\n\r");
                }
            
    printf ("\nBody (%lu bytes)\n\r",(unsigned long) res->get_body_length());
}

bool compare(const char* tok,const char* base){
    if(strstr(base,tok)==NULL){
        return false;
    }
    else{
        return true;
        }
}
int compare_in_db(const char* token,const char* base){
    const char * pch;
    const char* temp;
    int num_to_int=0;
    //if(strcmp(QR_MASTER,tok)==0) return 100;
    pch = strstr(base,token);  
    if(pch==NULL){
        return 0;
    }
    else{
        temp = strtok((char*)pch+36,"\""); //desplazo hasta la posicion 10 para llegar al digito de validacion
        //printf("%s",temp);
        num_to_int =atoi(temp);
        return num_to_int;
        }
}

void deleteTokens(NetworkInterface *network){
    //printf("Task delete tokens waiting for any flag from Event Mailbox.\r\n");
    while (true) {
                uint32_t flags_read=0;
                flags_read = isConnected.wait_any(connectedFlag,osWaitForever,true);
         while(!mail_box.empty()){ 
        
                osEvent evt = mail_box.get();
                if (evt.status == osEventMail) {
                                        mail_t *mail = (mail_t*)evt.value.p;
                                        printf("-----------TOKEN A ELIMINAR------\n\r");
                                        printf("\nTOKEN: %s \n\r"   , mail->token);
                                        printf("Direction: %d\n\r"  , mail->direction);
                                        printf("-----------------------------------\n\r");
                                        
                                     if(con_status==1)   {//if connected
                                           // printf("\n----- HTTPS POST request -----\n\r");
                                           #if GHOU_CONCENTRATOR_TYPE == DEVELOPMENT
                                            HttpsRequest* post_req_dt = new HttpsRequest(network, SSL_CA_PEM_DEV, HTTP_POST, "https://development.ghou.com.ec/api/customer/invitations/register_pass/");
                                            #else
                                            HttpsRequest* post_req_dt = new HttpsRequest(network, SSL_CA_PEM_PROD, HTTP_POST, "https://www.ghou.com.ec/api/customer/invitations/register_pass/");
                                            #endif
                                            post_req_dt->set_header("Content-Type", "application/json");
                                            char sbuffer[200];
                                                if(mail->direction==ENTRADA){
                                                sprintf(sbuffer,"{\"identification\":\"%s\",\"device_type\": \"QR\",\"direction\": \"E\",\"access_token\":\"%s2\"}\0",GHOU_ID,mail->token);
                                                }
                                                else if(mail->direction==SALIDA){
                                                sprintf(sbuffer,"{\"identification\":\"%s\",\"device_type\": \"QR\",\"direction\": \"D\",\"access_token\":\"%s1\"}\0",GHOU_ID,mail->token);
                                                }
                                               // printf(sbuffer);
                                            delete_token.lock();   
                                            HttpResponse* post_res_dt = post_req_dt->send(sbuffer, strlen(sbuffer));
                                            delete_token.unlock();
                                                if (!post_res_dt || post_res_dt->get_status_code()!= RESPONSE_OK) {
                                                    printf("HttpRequest failed (error code %d)\n\r", post_req_dt->get_error());
                                                    goto offline_token;                                                }
                                                    
                                            //printf("\n----- HTTPS POST response DELETE TOKEN-----\n\r");
                                            mail_box.free(mail);
                                            offline_token:
                                            dump_response(post_res_dt);
                                            delete post_req_dt; 
                                        }
                                   
                                    }
                                    
                        }//while empty
                        isConnected.set(noconnectedFlag);
        }  //while general 
}

void https_get_thread(NetworkInterface *network){
                                mbed_trace_init(); 
        while(1){ 
                        // Create a TCP socket
                                // Create a TLS socket (which holds a TCPSocket)
                               led_server=!led_server;
                                printf("\n----- Setting up TLS connection -----\n\r");
                                //printf("Tramitando estado de conexion\r\n");
                                nsapi_error_t r;
                                TLSSocket* socket_https = new TLSSocket();
                                socket_https->set_timeout(SOCKET_DB_TIMEOUT);
                                if ((r = socket_https->open(network)) != NSAPI_ERROR_OK) {
                                    printf("TLS socket open failed (%d)\n", r);
                                    t_https=TIME_OFF;
                                    con_status=0;
                                    goto alive_task_get;
                                }
                                #if GHOU_CONCENTRATOR_TYPE == DEVELOPMENT
                                if ((r = socket_https->set_root_ca_cert(SSL_CA_PEM_DEV)) != NSAPI_ERROR_OK) {
                                #else
                                if ((r = socket_https->set_root_ca_cert(SSL_CA_PEM_PROD)) != NSAPI_ERROR_OK) {
                                #endif
                                    printf("TLS socket set_root_ca_cert failed (%d)\n", r);
                                    t_https=TIME_OFF;
                                    con_status=0;
                                    goto alive_task_get;
                                }
                                #if GHOU_CONCENTRATOR_TYPE == DEVELOPMENT
                                if ((r = socket_https->connect("development.ghou.com.ec", EXTERNAL_SERVER_PORT)) != NSAPI_ERROR_OK) {
                                #else
                                if ((r = socket_https->connect("www.ghou.com.ec", EXTERNAL_SERVER_PORT)) != NSAPI_ERROR_OK) {
                                #endif
                                    printf("TLS socket connect failed (%d)\n", r);
                                    t_https=TIME_OFF;
                                    con_status=0;
                                    goto alive_task_get;
                                }
                              #if GHOU_CONCENTRATOR_TYPE == DEVELOPMENT
                             printf("Conectado a development.ghou.com.ec\n\r"); 
                             #else 
                             printf("Conectado a www.ghou.com.ec\n\r");
                             #endif                     
                             con_status=1;         
                        if(con_status==1){
                                        //printf("\n----- HTTPS POST request REFRESH DATABASE-----\n\r");
                                        #if GHOU_CONCENTRATOR_TYPE == DEVELOPMENT
                                        HttpsRequest* post_req = new HttpsRequest(socket_https, HTTP_POST, "https://development.ghou.com.ec/api/customer/invitations/tokens/");
                                        #else 
                                        HttpsRequest* post_req = new HttpsRequest(socket_https, HTTP_POST, "https://www.ghou.com.ec/api/customer/invitations/tokens/");
                                        #endif
                                        post_req->set_header("Content-Type", "application/json");
                                        char body[100];
                                        sprintf(body,"{\"identification\":\"%s\",\"device_type\": \"QR\"}\0",GHOU_ID);
                                        //const char body[] = "{\"identification\":\"77fa3533-fcf9-457e-9f29-57056ce6332a\",\"device_type\": \"QR\"}";
                                        
                                        HttpResponse* post_res_db = post_req->send(body, strlen(body));
                                        if (!post_res_db) {
                                            printf("HttpRequest failed (error code %d)\n", post_req->get_error());
                                            t_https=TIME_OFF;
                                            con_status=0;
                                            goto alive_task_post;
                                        }
                                        //printf("\n----- HTTPS POST response REFRESH DATABASE -----\n\r");
                                        //dump_response_db(post_res);
                                        dump_response_db(post_res_db);
                                        alive_task_post:
                                        delete post_req;
                                        isConnected.set(connectedFlag);    
                                        }
                                        
                        t_https=TIME_REQUEST;                
                        alive_task_get:
                        socket_https->close();
                        delete socket_https; 
                        led_server=!led_server;
                        wait(t_https);
                }      
        
}
void server_thread(NetworkInterface *network){
            TCPServer* server = new TCPServer();
            server->open(network);                          // Open the server on ethernet stack
            server->bind(network->get_ip_address(),INTERNAL_SERVER_PORT);   // Bind the HTTP port (TCP 80) to the server
            server->listen(MAX_CONNECTIONS);                              // Can handle 1 simultaneous connections 
        while (true) {                      //listening for http request
            TCPSocket *clt_sock = new TCPSocket(); // Q: when should these be cleared? When not connected anymore?
            SocketAddress clt_addr;
            clt_sock->set_timeout(SOCKET_SERVER_TIMEOUT);
            nsapi_error_t accept_res = server->accept(clt_sock, &clt_addr);
            // printf("IP Address: %s Port: %d\n\r", clt_addr.get_ip_address(), clt_addr.get_port());
        if (accept_res == NSAPI_ERROR_OK) {
            /*parser*//////////////////////////////////////////////////////////////////////////////////////////////
            // UNSAFE: should Mutex around it or something

            // needs to keep running until the socket gets closed
            while (1) {
            ParsedHttpRequest* response = new ParsedHttpRequest();
            HttpParser* parser = new HttpParser(response, HTTP_REQUEST);

            // Set up a receive buffer (on the heap)
            uint8_t* recv_buffer = (uint8_t*)malloc(HTTP_RECEIVE_BUFFER_SIZE);

            // TCPSocket::recv is called until we don't have any data anymore
            nsapi_size_or_error_t recv_ret;
            while ((recv_ret = clt_sock->recv(recv_buffer, HTTP_RECEIVE_BUFFER_SIZE)) > 0) {
                                                        // Pass the chunk into the http_parser
                                                        size_t nparsed = parser->execute((const char*)recv_buffer, recv_ret);
                                                        if (nparsed != recv_ret) {
                                                                                printf("Parsing failed... parsed %d bytes, received %d bytes\n", nparsed, recv_ret);
                                                                                recv_ret = -2101;
                                                                                break;
                                                                                }
                                        
                                                        if (response->is_message_complete()) {
                                                                                //printf("mensaje completo");
                                                                                break;
                                                                                }
                                                            }
                                                            // error?
                                                        if (recv_ret <= 0) {
                                                                                if (recv_ret < 0) {
                                                                                                //printf("Error reading from socket %d\n", recv_ret);
                                                                                                }
                                                
                                                                                // error = recv_ret;
                                                                                delete response;
                                                                                delete parser;
                                                                                free(recv_buffer);
                                                                                
                                                                                // q; should we always break out of the thread or only if NO_SOCKET ?
                                                                                // should we delete socket here? the socket seems already gone...
                                                                                if (recv_ret < -3000 || recv_ret == 0) {
                                                                                                                        //printf("Codigo basura borrar", recv_ret);
                                                                                                                        }
                                                                                else {
                                                                                            continue;
                                                                                        }
                                                                                        
                                                                                break; 
                                                                                }

                                                        // When done, call parser.finish()
                                                        parser->finish();
                                            
                                                        // Free the receive buffer
                                                        free(recv_buffer);

            // Let user application handle the request, if user needs a handle to response they need to memcpy themselves
            if (recv_ret > 0) {
///////////////////////////////////////////////////////////////desde aqui contesta el server/////////////////////////////////////////////////////////////////////////////////
                                char *token;
                                const char id[2]="=";
                                int q=0;
                                printf("Request ingresando: %s %s\n\r", http_method_str(response->get_method()), response->get_url().c_str());
                                /* get the first token 86 caracateres*/
                                q=strlen(response->get_url().c_str());                     
                                                        
                                    if (response->get_method() == HTTP_GET && response->get_url() == "/") {
                                                                                                        HttpResponseBuilder builder(RESPONSE_OK);
                                                                                                        builder.set_header("Content-Type", "text/html; charset=utf-8");
                                                                                                        char response[] = "<html><head><title>Controladora GHOU/QR</title></head>"
                                                                                                            "<body>"
                                                                                                                "<h1>No se que hace aqui joven</h1>"
                                                                                                                "<button id=\"toggle\">Cambiar LED ?</button>"
                                                                                                                "<button id=\"RELE1\">RELE 1 </button>"
                                                                                                                "<button id=\"RELE2\">RELE 2 </button>"
                                                                                                                #ifdef CONCENTRADOR_MULTIPLE
                                                                                                                "<button id=\"RELE3\">RELE 3 </button>"
                                                                                                                "<button id=\"RELE4\">RELE 4 </button>"
                                                                                                                #endif
                                                                                                                "<script>document.querySelector('#toggle').onclick = function() {"
                                                                                                                    "var x = new XMLHttpRequest(); x.open('POST', '/toggle'); x.send();"
                                                                                                                "}</script>"
                                                                                                                "<script>document.querySelector('#RELE1').onclick = function(){"
                                                                                                                    "var y = new XMLHttpRequest(); y.open('GET','/RELE1'); y.send();"
                                                                                                                "}</script>"
                                                                                                                "<script>document.querySelector('#RELE2').onclick = function(){"
                                                                                                                    "var z = new XMLHttpRequest(); z.open('GET','/RELE2'); z.send();"
                                                                                                                "}</script>"
                                                                                                                #ifdef CONCENTRADOR_MULTIPLE
                                                                                                                "<script>document.querySelector('#RELE3').onclick = function(){"
                                                                                                                    "var a = new XMLHttpRequest(); a.open('GET','/RELE3'); a.send();"
                                                                                                                "}</script>"
                                                                                                                "<script>document.querySelector('#RELE4').onclick = function(){"
                                                                                                                    "var b = new XMLHttpRequest(); b.open('GET','/RELE4'); b.send();"
                                                                                                                "}</script>"
                                                                                                                #endif
                                                                                                            "</body></html>";
                                                                                                        builder.send(clt_sock, response, sizeof(response) - 1);
                                                                                                            }
                                    else if (response->get_method() == HTTP_POST && response->get_url() == "/toggle") {
                                                                                    //printf("toggle LED called\n\r");
                                                                                    led = !led; 
                                                                                    HttpResponseBuilder builder(RESPONSE_OK);
                                                                                    builder.send(clt_sock, NULL, 0);
                                                                                    }                                                                                   
                                    else if (response->get_method() == HTTP_GET && response->get_url() == "/RELE1") {
                                                                                    led = !led;        
                                                                                    HttpResponseBuilder builder(RESPONSE_OK);
                                                                                    builder.send(clt_sock, NULL, 0);
                                                                                    RELE1.write(RELE_STATUS_HIGH);              // set RELE1 pin to high
                                                                                    wait(RELE_TIME_INTERVAL);
                                                                                    RELE1.write(!RELE_STATUS_HIGH);             // set RELE1 pin to low
                                                                                    led = !led;
                                                                                    }
                                    else if (response->get_method() == HTTP_GET && response->get_url() == "/RELE2") {
                                                                                    led = !led;        
                                                                                    HttpResponseBuilder builder(RESPONSE_OK);
                                                                                    builder.send(clt_sock, NULL, 0);
                                                                                    RELE2.write(RELE_STATUS_HIGH);             // set RELE2 pin to high
                                                                                    wait(RELE_TIME_INTERVAL);
                                                                                    RELE2.write(!RELE_STATUS_HIGH);             // set RELE1 pin to low
                                                                                    led = !led;
                                        }
                                    #ifdef CONCENTRADOR_MULTIPLE
                                    else if (response->get_method() == HTTP_GET && response->get_url() == "/RELE3") {
                                                                                    led = !led;        
                                                                                    HttpResponseBuilder builder(RESPONSE_OK);
                                                                                    builder.send(clt_sock, NULL, 0);
                                                                                    RELE3.write(RELE_STATUS_HIGH);             // set RELE2 pin to high
                                                                                    wait(RELE_TIME_INTERVAL);
                                                                                    RELE3.write(!RELE_STATUS_HIGH);             // set RELE1 pin to low 
                                                                                    led = !led;
                                        }
                                    else if (response->get_method() == HTTP_GET && response->get_url() == "/RELE4") {
                                                                                    led = !led;        
                                                                                    HttpResponseBuilder builder(RESPONSE_OK);
                                                                                    builder.send(clt_sock, NULL, 0);
                                                                                    RELE4.write(RELE_STATUS_HIGH);             // set RELE2 pin to high
                                                                                    wait(RELE_TIME_INTERVAL);
                                                                                    RELE4.write(!RELE_STATUS_HIGH);             // set RELE1 pin to low 
                                                                                    led = !led;
                                        }
                                    #endif                                                                                
                                    else if(response->get_method() == HTTP_GET && q == REQUEST_T) {
                                                                            char* uuid=NULL;
                                                                            char* id=NULL;
                                                                            char* pch=NULL;
                                                                            int count=0;
                                                                //parsing el token y el id del dispositivo que lo envio 
                                                                            pch = strtok ((char*)response->get_url().c_str(),"/");
                                                                                  while (pch != NULL)
                                                                                                  {
                                                                                                   if(count==0) id=pch;
                                                                                                   if(count==1) uuid=pch;
                                                                                                    //printf ("%s\n",pch);
                                                                                                    pch = strtok (NULL, "/");
                                                                                                    count++;
                                                                                                    }    
                                                                                                    printf ("ID: %s UUID: %s\n",id,uuid);
                                                                                      
                                                                //hasta aqui el parseo

                                                                if(compare(id,QR_IN_1)){//compara el id del dispositivo de entrada si esta registrado en el controlador
                                                                                    //compara el uuid
                                                                                    int k=0;  
                                                                                    k=compare_in_db(uuid,j); //ya no comparamos con j que era un puntero si no con un char array
                                                                                    kpt=&k;
                                                                                    if(k==3 || k==7){
                                                                                                        //printf("valor de k %d",k);
                                                                                                                            strcpy(delete_uuid,""); 
                                                                                                                            strcpy(delete_uuid,uuid);                    
                                                                                                                            RELE1.write(RELE_STATUS_HIGH);
                                                                                                                            //RELE2.write(0);         
                                                                                                                            wait(RELE_TIME_INTERVAL);
                                                                                                                            RELE1.write(!RELE_STATUS_HIGH);
                                                                                                                            HttpResponseBuilder builder(RESPONSE_OK);
                                                                                                                            builder.send(clt_sock, NULL, 0);
                                                                                                                            //event_qr.set(qrInEvent);
                                                                                                                            mail_t *mail = mail_box.alloc();
                                                                                                                            strcpy(mail->token,delete_uuid);
                                                                                                                            //direction 2 es entrada y 1 salida
                                                                                                                            mail->direction = ENTRADA;
                                                                                                                            mail_box.put(mail);
                                                                                                                                                                          
                                                                                                        }
                                                                                   /*  if(k=100){                        RELE1.write(0);   //QR MASTER      
                                                                                                                            wait(0.1);
                                                                                                                            RELE1.write(1);
                                                                                                                            HttpResponseBuilder builder(200);
                                                                                                                            builder.send(clt_sock, NULL, 0);
                                                                                                         }                   
                                                                                               */             
                                                                                     else {printf("TOKEN NO EXISTE...\n\r");HttpResponseBuilder builder(404);builder.send(clt_sock, NULL, 0);}                   
                                                                                                        
                                                                                        }
                                                                 if(compare(id,QR_OUT_1)){
                                                                                    int k=0;
                                                                                    k=compare_in_db(uuid,j);
                                                                                    if(k==1 || k==3 || k==7){
                                                                                                        //printf("valor de k %d",k);
                                                                                                                            strcpy(delete_uuid,""); 
                                                                                                                            strcpy(delete_uuid,uuid);                    
                                                                                                                            RELE2.write(RELE_STATUS_HIGH);     
                                                                                                                            wait(RELE_TIME_INTERVAL);
                                                                                                                            RELE2.write(!RELE_STATUS_HIGH);
                                                                                                                            HttpResponseBuilder builder(RESPONSE_OK);
                                                                                                                            builder.send(clt_sock, NULL, 0);
                                                                                                                            //event_qr.set(qrOutEvent);
                                                                                                                            mail_t *mail = mail_box.alloc();
                                                                                                                            strcpy(mail->token,delete_uuid);
                                                                                                                            //direction 2 es entrada y 1 salida
                                                                                                                            mail->direction = SALIDA;
                                                                                                                            mail_box.put(mail);
                                                                                                                           
                                                                                                                                              
                                                                                                        }
                                                                                       /*if(k=100){                        RELE1.write(0);   //QR MASTER      
                                                                                                                            wait(0.1);
                                                                                                                            RELE1.write(1);
                                                                                                                            HttpResponseBuilder builder(200);
                                                                                                                            builder.send(clt_sock, NULL, 0);
                                                                                                         }  
                                                                                            */                               
                                                                                      else{printf("TOKEN NO EXISTE...\n\r");HttpResponseBuilder builder(RESPONSE_FAIL);builder.send(clt_sock, NULL, 0);}                  
                                                                                        }
                                                                #ifdef CONCENTRADOR_MULTIPLE
                                                                if(compare(id,QR_IN_2)){
                                                                                    int k=0;
                                                                                    k=compare_in_db(uuid,j);
                                                                                    if(k==3 || k==7){
                                                                                                        //printf("valor de k %d",k);
                                                                                                                            strcpy(delete_uuid,""); 
                                                                                                                            strcpy(delete_uuid,uuid);                    
                                                                                                                            RELE3.write(RELE_STATUS_HIGH);     
                                                                                                                            wait(RELE_TIME_INTERVAL);
                                                                                                                            RELE3.write(!RELE_STATUS_HIGH);
                                                                                                                            HttpResponseBuilder builder(RESPONSE_OK);
                                                                                                                            builder.send(clt_sock, NULL, 0);
                                                                                                                            //event_qr.set(qrOutEvent);
                                                                                                                            mail_t *mail = mail_box.alloc();
                                                                                                                            strcpy(mail->token,delete_uuid);
                                                                                                                            //direction 2 es entrada y 1 salida
                                                                                                                            mail->direction = ENTRADA;
                                                                                                                            mail_box.put(mail);
                                                                                                                           
                                                                                                                                              
                                                                                                        }
                                                                                       /*if(k=100){                        RELE1.write(0);   //QR MASTER      
                                                                                                                            wait(0.1);
                                                                                                                            RELE1.write(1);
                                                                                                                            HttpResponseBuilder builder(200);
                                                                                                                            builder.send(clt_sock, NULL, 0);
                                                                                                         }  
                                                                                            */                               
                                                                                      else{printf("TOKEN NO EXISTE...\n\r");HttpResponseBuilder builder(RESPONSE_FAIL);builder.send(clt_sock, NULL, 0);}                  
                                                                                        }
                                                                if(compare(id,QR_OUT_2)){
                                                                                    int k=0;
                                                                                    k=compare_in_db(uuid,j);
                                                                                    if(k==1 || k==3 || k==7){
                                                                                                        //printf("valor de k %d",k);
                                                                                                                            strcpy(delete_uuid,""); 
                                                                                                                            strcpy(delete_uuid,uuid);                    
                                                                                                                            RELE4.write(RELE_STATUS_HIGH);     
                                                                                                                            wait(RELE_TIME_INTERVAL);
                                                                                                                            RELE4.write(!RELE_STATUS_HIGH);
                                                                                                                            HttpResponseBuilder builder(RESPONSE_OK);
                                                                                                                            builder.send(clt_sock, NULL, 0);
                                                                                                                            //event_qr.set(qrOutEvent);
                                                                                                                            mail_t *mail = mail_box.alloc();
                                                                                                                            strcpy(mail->token,delete_uuid);
                                                                                                                            //direction 2 es entrada y 1 salida
                                                                                                                            mail->direction = SALIDA;
                                                                                                                            mail_box.put(mail);
                                                                                                                           
                                                                                                                                              
                                                                                                        }
                                                                                       /*if(k=100){                        RELE1.write(0);   //QR MASTER      
                                                                                                                            wait(0.1);
                                                                                                                            RELE1.write(1);
                                                                                                                            HttpResponseBuilder builder(200);
                                                                                                                            builder.send(clt_sock, NULL, 0);
                                                                                                         }  
                                                                                            */                               
                                                                                      else{printf("TOKEN NO EXISTE...\n\r");HttpResponseBuilder builder(RESPONSE_FAIL);builder.send(clt_sock, NULL, 0);}                  
                                                                                        } 
                                                                #endif                            
                                                                //else{printf("DISPOSITIVO NO PERMITIDO...\n\r");HttpResponseBuilder builder(404);builder.send(clt_sock, NULL, 0);}                           
                                                                                        
                                                                                 
                                                    } //FIN:else if(response->get_method() == HTTP_GET && q == 74) 
                                    else {
                                                                                    HttpResponseBuilder builder(RESPONSE_FAIL);
                                                                                    builder.send(clt_sock, NULL, 0);    
                                         }                                
                                
                                }

            // Free the response and parser
            delete response;
            delete parser;
            clt_sock->close();
            //printf("borrado respuesta y parser");
            }  
                    /*---------------------------------------------fin----if (accept_res == NSAPI_ERROR_OK)-----------------------------------------------*/
                    }
                delete clt_sock; 
                clt_addr=NULL;  
                //printf("socket borrado\n\r");
                }  
    }
 
       
void led2_thread() {
    while (true) {
        led_thread = !led_thread;
        wait(HEARTBEAT);
    }
}
 
int main() {
    pc.baud(BAUD_SET);
    RELE1.write(RELE_STATUS_LOW);
    RELE2.write(RELE_STATUS_LOW);
    #ifdef CONCENTRADOR_MULTIPLE
    RELE3.write(RELE_STATUS_LOW);
    RELE4.write(RELE_STATUS_LOW);
    #endif
    //Clear the currently stored error info in Crash-data-RAM
    //mbed_reset_reboot_count();
    // Connect to the network (see mbed_app.json for the connectivity method used)
    NetworkInterface* network = easy_connect(true);
    if (!network) {
        printf("No se puede conectar a la red, mire la salida serial\n\r");
        void system_reset();
    }
    
    thread_server.start(callback(server_thread,network));
    thread_server.set_priority(osPriorityBelowNormal);
    
    thread_led.start(led2_thread);
    
    thread_httpsGet.start(callback(https_get_thread,network));
    thread_httpsGet.set_priority(osPriorityRealtime); 
    
    thread_deleteTokens.start(callback(deleteTokens,network));
    thread_deleteTokens.set_priority(osPriorityHigh5);
   
    wait(osWaitForever);
    printf("Algun error ha sucedido\n\r");
    
}
