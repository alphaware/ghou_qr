#ifndef __GHOU_SETUP_H__
#define __GHOU_SETUP_H__

//#define XPANSIONS_NUMBER 1


//Definitions 
#define DEVELOPMENT             0
#define PRODUCTION              1
#define noconnectedFlag         0
#define connectedFlag           1
#define RELE_STATUS_HIGH        0       //Estado de RELE alto  - logica inversa
#define RELE_STATUS_LOW         1       //Estado de RELE bajo - logica inversa
#define SOCKET_SERVER_TIMEOUT   1000    //tiempo maximo de respuesta del servidor INTERNO.
#define SOCKET_DB_TIMEOUT       1500    //tiempo maximo que espera el socket para dar perdida una conexion en la tarea de actualizar la base de datos
#define REQUEST_T               74      //tamano del request proveniente del driver qr, los que no cumplan con este tamano son descartados
#define TIME_REQUEST            12      //tiempo entre request al server para actualizar la data base
#define TIME_OFF                2       //tiempo de tarea cuando no hay internet
#define INV_SIZE                391
#define DB_SIZE                 75004   // tamano de la base de datos interna para tokens
#define RELE_TIME_INTERVAL      0.3     //tiempo en segundos que el rele demora de pasar de un estado a otro (ALTO-BAJO)
#define TOKEN_LENGTH            37      // longitud del token completo
#define RELE_1                  D6      //pinout al cual esta conectado el rele 1 ENTRADA 1
#define RELE_2                  D7      //pinout al cual esta conectado el rele 2 SALIDA 1
#define RELE_3                  D8      //pinout al cual esta conectado el rele 1 ENTRADA 2
#define RELE_4                  D9      //pinout al cual esta conectado el rele 2 SALIDA 2
#define TOKEN_MAIL_MAX          100     //mumero maximo de invitaciones que puede almecenar temporalmente en modo offline sin SD
#define INTERNAL_SERVER_PORT    3000
#define EXTERNAL_SERVER_PORT    443
#define MAX_CONNECTIONS         5
#define RESPONSE_OK             200
#define RESPONSE_FAIL           404
#define HEARTBEAT               0.5
#define BAUD_SET                57600
#define SALIDA                  1
#define ENTRADA                 2



//Definir si el controlador es de desarrollo o de produccion
//#define GHOU_CONCENTRATOR_TYPE DEVELOPMENT
#define GHOU_CONCENTRATOR_TYPE PRODUCTION
//#define CONCENTRADOR_MULTIPLE       1       //maximo hasta 2



//IPs de conexion a red
#define MY_IP           "192.168.0.9"
#define MY_GATEWAY      "192.168.0.1"
#define MY_MASK         "255.255.255.0"
//Identificadores de ghou para la nube y red interna (interdispositivos)
#define GHOU_ID          "1a96f958-72ef-4981-9a8b-f6fce3592b18"
#define QR_OUT_1         "f023016e-cb6a-4c27-b051-9081416f6283"
#define QR_IN_1          "f4e44b56-28da-4d3b-b0cc-9e1d3c0a15d6"
#ifdef CONCENTRADOR_MULTIPLE
#define QR_IN_2          "f4e44b56-28da-4d3b-b0cc-9e1d3c0a15d6"
#define QR_OUT_2         "f023016e-cb6a-4c27-b051-9081416f6283"
#endif

/* List of trusted root CA certificates
 * currently two: Amazon, the CA for os.mbed.com and Let's Encrypt, the CA for httpbin.org
 *
 * To add more root certificates, just concatenate them.
 */
const char SSL_CA_PEM_PROD_OLD[] = 
"-----BEGIN CERTIFICATE-----\n"
"MIIEkjCCA3qgAwIBAgIQCgFBQgAAAVOFc2oLheynCDANBgkqhkiG9w0BAQsFADA/\n"
"MSQwIgYDVQQKExtEaWdpdGFsIFNpZ25hdHVyZSBUcnVzdCBDby4xFzAVBgNVBAMT\n"
"DkRTVCBSb290IENBIFgzMB4XDTE2MDMxNzE2NDA0NloXDTIxMDMxNzE2NDA0Nlow\n"
"SjELMAkGA1UEBhMCVVMxFjAUBgNVBAoTDUxldCdzIEVuY3J5cHQxIzAhBgNVBAMT\n"
"GkxldCdzIEVuY3J5cHQgQXV0aG9yaXR5IFgzMIIBIjANBgkqhkiG9w0BAQEFAAOC\n"
"AQ8AMIIBCgKCAQEAnNMM8FrlLke3cl03g7NoYzDq1zUmGSXhvb418XCSL7e4S0EF\n"
"q6meNQhY7LEqxGiHC6PjdeTm86dicbp5gWAf15Gan/PQeGdxyGkOlZHP/uaZ6WA8\n"
"SMx+yk13EiSdRxta67nsHjcAHJyse6cF6s5K671B5TaYucv9bTyWaN8jKkKQDIZ0\n"
"Z8h/pZq4UmEUEz9l6YKHy9v6Dlb2honzhT+Xhq+w3Brvaw2VFn3EK6BlspkENnWA\n"
"a6xK8xuQSXgvopZPKiAlKQTGdMDQMc2PMTiVFrqoM7hD8bEfwzB/onkxEz0tNvjj\n"
"/PIzark5McWvxI0NHWQWM6r6hCm21AvA2H3DkwIDAQABo4IBfTCCAXkwEgYDVR0T\n"
"AQH/BAgwBgEB/wIBADAOBgNVHQ8BAf8EBAMCAYYwfwYIKwYBBQUHAQEEczBxMDIG\n"
"CCsGAQUFBzABhiZodHRwOi8vaXNyZy50cnVzdGlkLm9jc3AuaWRlbnRydXN0LmNv\n"
"bTA7BggrBgEFBQcwAoYvaHR0cDovL2FwcHMuaWRlbnRydXN0LmNvbS9yb290cy9k\n"
"c3Ryb290Y2F4My5wN2MwHwYDVR0jBBgwFoAUxKexpHsscfrb4UuQdf/EFWCFiRAw\n"
"VAYDVR0gBE0wSzAIBgZngQwBAgEwPwYLKwYBBAGC3xMBAQEwMDAuBggrBgEFBQcC\n"
"ARYiaHR0cDovL2Nwcy5yb290LXgxLmxldHNlbmNyeXB0Lm9yZzA8BgNVHR8ENTAz\n"
"MDGgL6AthitodHRwOi8vY3JsLmlkZW50cnVzdC5jb20vRFNUUk9PVENBWDNDUkwu\n"
"Y3JsMB0GA1UdDgQWBBSoSmpjBH3duubRObemRWXv86jsoTANBgkqhkiG9w0BAQsF\n"
"AAOCAQEA3TPXEfNjWDjdGBX7CVW+dla5cEilaUcne8IkCJLxWh9KEik3JHRRHGJo\n"
"uM2VcGfl96S8TihRzZvoroed6ti6WqEBmtzw3Wodatg+VyOeph4EYpr/1wXKtx8/\n"
"wApIvJSwtmVi4MFU5aMqrSDE6ea73Mj2tcMyo5jMd6jmeWUHK8so/joWUoHOUgwu\n"
"X4Po1QYz+3dszkDqMp4fklxBwXRsW10KXzPMTZ+sOPAveyxindmjkW8lGy+QsRlG\n"
"PfZ+G6Z6h7mjem0Y+iWlkYcV4PIWL1iwBi8saCbGS5jN2p8M+X+Q7UNKEkROb3N6\n"
"KOqkqm57TH2H3eDJAkSnh6/DNFu0Qg==\n"
"-----END CERTIFICATE-----\n";
const char SSL_CA_PEM_PROD[]=
"-----BEGIN CERTIFICATE-----\n"
"MIIEZTCCA02gAwIBAgIQQAF1BIMUpMghjISpDBbN3zANBgkqhkiG9w0BAQsFADA/\n"
"MSQwIgYDVQQKExtEaWdpdGFsIFNpZ25hdHVyZSBUcnVzdCBDby4xFzAVBgNVBAMT\n"
"DkRTVCBSb290IENBIFgzMB4XDTIwMTAwNzE5MjE0MFoXDTIxMDkyOTE5MjE0MFow\n"
"MjELMAkGA1UEBhMCVVMxFjAUBgNVBAoTDUxldCdzIEVuY3J5cHQxCzAJBgNVBAMT\n"
"AlIzMIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAuwIVKMz2oJTTDxLs\n"
"jVWSw/iC8ZmmekKIp10mqrUrucVMsa+Oa/l1yKPXD0eUFFU1V4yeqKI5GfWCPEKp\n"
"Tm71O8Mu243AsFzzWTjn7c9p8FoLG77AlCQlh/o3cbMT5xys4Zvv2+Q7RVJFlqnB\n"
"U840yFLuta7tj95gcOKlVKu2bQ6XpUA0ayvTvGbrZjR8+muLj1cpmfgwF126cm/7\n"
"gcWt0oZYPRfH5wm78Sv3htzB2nFd1EbjzK0lwYi8YGd1ZrPxGPeiXOZT/zqItkel\n"
"/xMY6pgJdz+dU/nPAeX1pnAXFK9jpP+Zs5Od3FOnBv5IhR2haa4ldbsTzFID9e1R\n"
"oYvbFQIDAQABo4IBaDCCAWQwEgYDVR0TAQH/BAgwBgEB/wIBADAOBgNVHQ8BAf8E\n"
"BAMCAYYwSwYIKwYBBQUHAQEEPzA9MDsGCCsGAQUFBzAChi9odHRwOi8vYXBwcy5p\n"
"ZGVudHJ1c3QuY29tL3Jvb3RzL2RzdHJvb3RjYXgzLnA3YzAfBgNVHSMEGDAWgBTE\n"
"p7Gkeyxx+tvhS5B1/8QVYIWJEDBUBgNVHSAETTBLMAgGBmeBDAECATA/BgsrBgEE\n"
"AYLfEwEBATAwMC4GCCsGAQUFBwIBFiJodHRwOi8vY3BzLnJvb3QteDEubGV0c2Vu\n"
"Y3J5cHQub3JnMDwGA1UdHwQ1MDMwMaAvoC2GK2h0dHA6Ly9jcmwuaWRlbnRydXN0\n"
"LmNvbS9EU1RST09UQ0FYM0NSTC5jcmwwHQYDVR0OBBYEFBQusxe3WFbLrlAJQOYf\n"
"r52LFMLGMB0GA1UdJQQWMBQGCCsGAQUFBwMBBggrBgEFBQcDAjANBgkqhkiG9w0B\n"
"AQsFAAOCAQEA2UzgyfWEiDcx27sT4rP8i2tiEmxYt0l+PAK3qB8oYevO4C5z70kH\n"
"ejWEHx2taPDY/laBL21/WKZuNTYQHHPD5b1tXgHXbnL7KqC401dk5VvCadTQsvd8\n"
"S8MXjohyc9z9/G2948kLjmE6Flh9dDYrVYA9x2O+hEPGOaEOa1eePynBgPayvUfL\n"
"qjBstzLhWVQLGAkXXmNs+5ZnPBxzDJOLxhF2JIbeQAcH5H0tZrUlo5ZYyOqA7s9p\n"
"O5b85o3AM/OJ+CktFBQtfvBhcJVd9wvlwPsk+uyOy2HI7mNxKKgsBTt375teA2Tw\n"
"UdHkhVNcsAKX1H7GNNLOEADksd86wuoXvg==\n"
"-----END CERTIFICATE-----\n";

const char SSL_CA_PEM_DEV[]=
"-----BEGIN CERTIFICATE-----\n"
"MIIFFjCCAv6gAwIBAgIRAJErCErPDBinU/bWLiWnX1owDQYJKoZIhvcNAQELBQAw\n"
"TzELMAkGA1UEBhMCVVMxKTAnBgNVBAoTIEludGVybmV0IFNlY3VyaXR5IFJlc2Vh\n"
"cmNoIEdyb3VwMRUwEwYDVQQDEwxJU1JHIFJvb3QgWDEwHhcNMjAwOTA0MDAwMDAw\n"
"WhcNMjUwOTE1MTYwMDAwWjAyMQswCQYDVQQGEwJVUzEWMBQGA1UEChMNTGV0J3Mg\n"
"RW5jcnlwdDELMAkGA1UEAxMCUjMwggEiMA0GCSqGSIb3DQEBAQUAA4IBDwAwggEK\n"
"AoIBAQC7AhUozPaglNMPEuyNVZLD+ILxmaZ6QoinXSaqtSu5xUyxr45r+XXIo9cP\n"
"R5QUVTVXjJ6oojkZ9YI8QqlObvU7wy7bjcCwXPNZOOftz2nwWgsbvsCUJCWH+jdx\n"
"sxPnHKzhm+/b5DtFUkWWqcFTzjTIUu61ru2P3mBw4qVUq7ZtDpelQDRrK9O8Zutm\n"
"NHz6a4uPVymZ+DAXXbpyb/uBxa3Shlg9F8fnCbvxK/eG3MHacV3URuPMrSXBiLxg\n"
"Z3Vms/EY96Jc5lP/Ooi2R6X/ExjqmAl3P51T+c8B5fWmcBcUr2Ok/5mzk53cU6cG\n"
"/kiFHaFpriV1uxPMUgP17VGhi9sVAgMBAAGjggEIMIIBBDAOBgNVHQ8BAf8EBAMC\n"
"AYYwHQYDVR0lBBYwFAYIKwYBBQUHAwIGCCsGAQUFBwMBMBIGA1UdEwEB/wQIMAYB\n"
"Af8CAQAwHQYDVR0OBBYEFBQusxe3WFbLrlAJQOYfr52LFMLGMB8GA1UdIwQYMBaA\n"
"FHm0WeZ7tuXkAXOACIjIGlj26ZtuMDIGCCsGAQUFBwEBBCYwJDAiBggrBgEFBQcw\n"
"AoYWaHR0cDovL3gxLmkubGVuY3Iub3JnLzAnBgNVHR8EIDAeMBygGqAYhhZodHRw\n"
"Oi8veDEuYy5sZW5jci5vcmcvMCIGA1UdIAQbMBkwCAYGZ4EMAQIBMA0GCysGAQQB\n"
"gt8TAQEBMA0GCSqGSIb3DQEBCwUAA4ICAQCFyk5HPqP3hUSFvNVneLKYY611TR6W\n"
"PTNlclQtgaDqw+34IL9fzLdwALduO/ZelN7kIJ+m74uyA+eitRY8kc607TkC53wl\n"
"ikfmZW4/RvTZ8M6UK+5UzhK8jCdLuMGYL6KvzXGRSgi3yLgjewQtCPkIVz6D2QQz\n"
"CkcheAmCJ8MqyJu5zlzyZMjAvnnAT45tRAxekrsu94sQ4egdRCnbWSDtY7kh+BIm\n"
"lJNXoB1lBMEKIq4QDUOXoRgffuDghje1WrG9ML+Hbisq/yFOGwXD9RiX8F6sw6W4\n"
"avAuvDszue5L3sz85K+EC4Y/wFVDNvZo4TYXao6Z0f+lQKc0t8DQYzk1OXVu8rp2\n"
"yJMC6alLbBfODALZvYH7n7do1AZls4I9d1P4jnkDrQoxB3UqQ9hVl3LEKQ73xF1O\n"
"yK5GhDDX8oVfGKF5u+decIsH4YaTw7mP3GFxJSqv3+0lUFJoi5Lc5da149p90Ids\n"
"hCExroL1+7mryIkXPeFM5TgO9r0rvZaBFOvV2z0gp35Z0+L4WPlbuEjN/lxPFin+\n"
"HlUjr8gRsI3qfJOQFy/9rKIJR0Y/8Omwt/8oTWgy1mdeHmmjk7j1nYsvC9JSQ6Zv\n"
"MldlTTKB3zhThV1+XWYp6rjd5JW1zbVWEkLNxE7GJThEUG3szgBVGP7pSWTUTsqX\n"
"nLRbwHOoq7hHwg==\n"
"-----END CERTIFICATE-----\n";

/* 
//Support 4 Extensions of 16 relays (64 relays)
#if XPANSIONS_NUMBER == 1
#define XS1          0x00
#define JD1        0x01
#define JD2        0x02
#define JD3        0x03
#define JD4        0x04
#define JD5        0x05
#define JD6        0x06
#define JD7        0x07
#define JD8        0x08
#define JD9        0x09
#define JD10       0x0A
#define JD11       0x0B
#define JD12       0x0C
#define JD13       0x0D
#define JD14       0x0E
#define JD15       0x0F
#define JD16       0x10
#elif XPANSIONS_NUMBER == 2
#define XS2          0x01
#define JD17       0x11
#define JD18       0x12 
#define JD19       0x13 
#define JD20       0x14
#define JD21       0x15
#define JD22       0x16
#define JD23       0x17
#define JD24       0x18
#define JD25       0x19
#define JD26       0x1A
#define JD27       0x1B
#define JD28       0x1C
#define JD29       0x1D
#define JD30       0x1E
#define JD31       0x1F
#define JD32       0x20


#elif XPANSIONS_NUMBER == 3
#define XS3          0x03
#define JD33       0x21
#define JD34       0x22
#define JD35       0x23
#define JD36       0x24
#define JD37       0x25
#define JD38       0x26
#define JD39       0x27
#define JD40       0x28
#define JD41       0x29
#define JD42       0x2A
#define JD43       0x2B
#define JD44       0x2C
#define JD45       0x2D
#define JD46       0x2E
#define JD47       0x2F
#define JD48       0x30

#elif  XPANSIONS_NUMBER ==4
#define XS4          0x04
#define JD49       0x31
#define JD50       0x32
#define JD51       0x33
#define JD52       0x34
#define JD53       0x35
#define JD54       0x36
#define JD55       0x37
#define JD56       0x38
#define JD57       0x39
#define JD58       0x3A
#define JD59       0x3B
#define JD60       0x3C
#define JD61       0x3D
#define JD62       0x3E
#define JD63       0x3F
#define JD64       0x40
#endif
//PISOS ARRANGEMENT

*/







#endif // 